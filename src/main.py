#you should run IsWf_model first and run workclf_model
import pickle
import make_IsWf_model
import make_workclf_model

PATH_workclf = "./models/workclf_model"
PATH_IsWfmodel = "./models/IsWf_model"

test_task = "내일까지 보고서 작성해주세요."

# IsfWfmodel 가지고 오는 함수
def get_IsfWfmodel():
    with open(PATH_IsWfmodel, 'rb') as f:
        loaded_model = pickle.load(f)

        return loaded_model
    
#workclfmodel가지고 오는 함수
def get_workclfmodel():
    with open(PATH_workclf, 'rb') as f:
        loaded_model = pickle.load(f)

        return loaded_model

def main():
    #두 개의 모델 가지고 옴
    IsWfmodel = get_IsfWfmodel()
    workclfmodel = get_workclfmodel()

    #두 모델의 word_index에 대한 토큰
    token_IsWf = make_IsWf_model.tokenize_text(test_task)
    token_workclf = make_workclf_model.tokenize_text(test_task)

    if IsWfmodel.predict(token_IsWf)[0]==1:
        print("this is wf")
        if workclfmodel.predict(token_workclf)[0]==1:
            print("요약")
        else:
            print("등록")
    else:
        print("not wf")

    

main()